/////////////////////////////////////////////////////////////////////////////
// FILE: _convergence.tol
// PURPOSE: Declares NameBlok BysInfDiag::Convergence
/////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
NameBlock Convergence =
//////////////////////////////////////////////////////////////////////////////
[[
  //read only autodoc
  Text _.autodoc.description =
  "Bayesian Convergence Diagnostics of MonteCarlo Markov Chains.";

////////////////////////////////////////////////////////////////////////////////
Set MannWhitneyUtest.FullDistrib.TwoSamples(
  Matrix Y1,      //First sample 
  Matrix Y2,      //Second sample 
  Real signif,    //Significance of test
  Real parts)     //Number of parts to divide the range of values
////////////////////////////////////////////////////////////////////////////////
{[[
  Real N1 = Rows(Y1);
  Real N2 = Rows(Y2);
  Real N12 = N1+N2;
  Matrix Y12 = If(N1==N2,Y1|Y2,Y1<<Y2);
  Real min = MatMin(Y12);
  Real max = MatMax(Y12);
  Real h = 1/(parts+1);
  Real len = (max-min)*h;
  Matrix p = SetRow(Range(h,parts*h,h));
  Matrix Q1 = Quantile(Y1,p);
  Matrix Q2 = Quantile(Y2,p);
  Matrix Q12 = Tra(Q1)|Tra(Q2);
  Matrix Q = Quantile(Y12,p);
  Set QL = MatSet(Q)[1];
  Set ev = For(1,parts,Set(Real k) {[[
  //Real k = 1;
    Matrix y1 = Abs(Y1-QL[k]);
    Matrix y2 = Abs(Y2-QL[k]);
    Matrix y12 = If(N1==N2,y1|y2,y1<<y2);
    Real mdn1 = MatMedian(y1);
    Real mdn2 = MatMedian(y2);
    Set MWUT = AlgLib.MannWhitneyUtest(y1, y2);
    Real pValue = MWUT[1];
    Real accept = pValue>signif
  ]]});
  Set ev.tr = Traspose(ev); 
  Real pValue = SetMin(ev.tr[Card(ev[1])-1]);
  Real accept = SetMin(ev.tr[Card(ev[1])  ])
]]}

/*
////////////////////////////////////////////////////////////////////////////////
Set MannWhitneyUtest.FullDistrib(
  Set Y,          //Samples 
  Real signif,    //Significance of test
  Real parts,     //Number of parts to divide the range of values
  Real numPerm)   //Number of random permutations to be checked (1, 2 or 3?)
////////////////////////////////////////////////////////////////////////////////
{
  
};
*/

]];
  


